<?php

namespace Drupal\commerce_cart_dialog\Form;

use Drupal\aggregator\Plugin\AggregatorPluginManager;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_cart_dialog\DialogSettingsHelper;

/**
 * Configures aggregator settings for this site.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The instantiated plugin instances that have configuration forms.
   *
   * @var \Drupal\Core\Plugin\PluginFormInterface[]
   */
  protected $configurableInstances = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_cart_dialog_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_cart_dialog.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_cart_dialog.settings');

    $form['dialog_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Dialog Settings'),
      '#description' => $this->t('Set the type of dialog and options to be used for openning the cart in a dialog.'),
      '#open' => TRUE,
    ];

    // $form['dialog_settings']['dialog_type'] = [
    //   '#type' => 'select',
    //   '#title' => $this->t('Dialog Type'),
    //   '#default_value' => $config->get('dialog_type'),
    //   '#options' => [
    //     'modal_dialog' => $this->t('Modal dialog'),
    //     'non_modal_dialog' => $this->t('Non-Modal dialog'),
    //     'off_canvas_dialog' => $this->t('Off canvas dialog'),
    //   ],
    //   '#description' => $this->t('<strong>Modal dialogs:</strong> They overlap the entire page, no other elements can be clicked while modal dialogs are visible. Only one modal popup can be opened at the same time.<br>
    //   <strong>Non modal dialogs:</strong> They pop up and stay on top of the page, but still other elements on the page can be clicked. Multiple dialogs can be displayed at the same time.<br>
    //   <strong>Off canvas dialogs:</strong> Are no popup windows that overlap other content, but are sliding into the page by moving other content to the side. This type of dialog is especially useful to display larger portions of content, like long detail pages that could require the user to scroll down. Also the off canvas dialog is well usable on mobile devices. '),
    // ];

    $form['dialog_settings']['dialog_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Dialog Options'),
      '#default_value' => $config->get('dialog_options'),
      '#description' => $this->t('Enter one value per line, in the format option|value. Refer <a target="_blank" href="https://api.jqueryui.com/dialog/">https://api.jqueryui.com/dialog/</a> for available options.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $options = DialogSettingsHelper::extractOptions($form_state->getValue('dialog_options'));
    if (!is_array($options)) {
      $form_state->setErrorByName('dialog_options', t('Dialog options: invalid input.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('commerce_cart_dialog.settings');

    // $config->set('dialog_type', $form_state->getValue('dialog_type'));
    $config->set('dialog_options', $form_state->getValue('dialog_options'));
    $config->save();
  }

}
