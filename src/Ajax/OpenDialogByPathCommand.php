<?php

namespace Drupal\commerce_cart_dialog\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;
use Drupal\Core\Ajax\CommandWithAttachedAssetsInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Component\Utility\Html;

/**
 * AJAX command for openning a dialog to show contents of the path.
 *
 * @ingroup ajax
 */
class OpenDialogByPathCommand implements CommandInterface, CommandWithAttachedAssetsInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * The path of the page to be opened in dialog.
   *
   * @var string
   */
  protected $href;

  /**
   * The dialog options array.
   *
   * @var array
   */
  protected $dialog_options;

  /**
   * The dialog renderer.
   *
   * @var string
   */
  protected $dialog_renderer;

  /**
   * The dialog type.
   *
   * @var string
   */
  protected $dialog_type;

  /**
   * Constructs an OpenDialogByPathCommand object.
   *
   * @param string $href
   *   The path of the page to be opened in dialog.
   * @param array $dialog_options
   *   The dialog options.
   * @param string $dialog_renderer
   *   The dialog renderer
   * @param string $dialog_type
   *   The dialog type
   */
  public function __construct($href, $dialog_options = [], $dialog_renderer = '', $dialog_type = 'modal') {
    $this->href = $href;
    $this->dialog_type = $dialog_type;
    $this->dialog_renderer = $dialog_renderer;
    $this->dialog_options = $dialog_options;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    // Please refer https://www.drupal.org/docs/drupal-apis/ajax-api/ajax-dialog-boxes
    // to know more about dialog boxes.
    return [
      'command' => 'openDialogByPath', // Command defined in js/open_dialog.js
      'href' => $this->href,
      'dialogType' => $this->dialog_type,
      'dialogRenderer' => $this->dialog_renderer,
      'dialogOptions' => $this->dialog_options,
      'htmlID' => Html::getUniqueId('open-dialog-auto-link'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedAssets() {
    $assets = new AttachedAssets();
    $assets->setLibraries(['commerce_cart_dialog/dialog']);
    return $assets;
  }

}
