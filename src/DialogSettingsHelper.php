<?php

namespace Drupal\commerce_cart_dialog;

use Drupal\Core\Config\ConfigFactoryInterface;

class DialogSettingsHelper {

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Configuration settings of this module.
   */
  protected $config;

  /**
   * Constructs a DialogSettingsHelper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('commerce_cart_dialog.settings');
  }

  public function getDialogType() {
    $dialog_type = $this->config->get('dialog_type');

    $type = 'modal';

    if ($dialog_type == 'modal_dialog') {
      $type = 'modal';
    }
    else if ($dialog_type == 'non_modal_dialog') {
      $type = 'dialog';
    }
    else if ($dialog_type == 'off_canvas_dialog') {
      $type = 'dialog';
    }
    return $type;
  }

  public function getDialogRenderer() {
    $dialog_type = $this->config->get('dialog_type');
    $renderer = '';
    if ($dialog_type == 'off_canvas_dialog') {
      $renderer = 'off_canvas';
    }
    return $renderer;
  }

  public function getDialogOptions() {
    $options = static::extractOptions($this->config->get('dialog_options'));
    $boolean_options = ['draggable', 'resizable'];
    foreach ($boolean_options as $option) {
      if (isset($options[$option])) {
        $options[$option] = (bool) $options[$option];
      }
    }

    return $options;
  }

  /**
   * Copy of Drupal\options\Plugin\Field\FieldType::extractAllowedValues() with modification
   */
  public static function extractOptions($string) {
    $values = [];

    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    $generated_keys = $explicit_keys = FALSE;
    foreach ($list as $position => $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
        $explicit_keys = TRUE;
      }
      else {
        return;
      }

      $values[$key] = $value;
    }

    // We generate keys only if the list contains no explicit key at all.
    if ($explicit_keys && $generated_keys) {
      return;
    }

    return $values;
  }
}