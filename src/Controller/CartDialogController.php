<?php

namespace Drupal\commerce_cart_dialog\Controller;

use Drupal\commerce_cart\Controller\CartController;

/**
 * Provides the cart page.
 */
class CartDialogController extends CartController {

  /**
   * {@inheritdoc}
   */
  public function cartPage() {
    // It is now mainly exact copy as in the Commerce Cart module.
    // Just on separate route to help modifying for enabling Ajax in commerce_cart_dialog_form_alter().
    return parent::cartPage();
  }
}
