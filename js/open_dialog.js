
(function ($, Drupal) {
  /**
   * See Drupal\commerce_cart_dialog\Ajax\OpenDialogByPathCommand, which prepare the response for this command.
   */
  Drupal.AjaxCommands.prototype.openDialogByPath = function (ajax, response, status) {
    let ajaxLink = document.createElement('a');
    ajaxLink.setAttribute("id", response.htmlID);

    var elementSettings = {
      progress: { type: 'throbber' },
      dialogType: response.dialogType,
      dialog: response.dialogOptions,
      dialogRenderer: response.dialogRenderer,
      base: response.htmlID,
      element: ajaxLink
    };
    var href = response.href;

    if (href) {
      elementSettings.url = href;
      elementSettings.event = 'click';
    }

    Drupal.ajax(elementSettings).execute();
  };
})(jQuery, Drupal);